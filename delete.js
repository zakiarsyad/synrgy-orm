const { Article } = require('./models')

const query = {
    where: {
        id: 4
    }
}

Article.destroy(query)
.then(article => {
    console.log(article)
})
.catch(err => {
    console.log(err)
})