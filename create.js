const { Article } = require('./models')

Article.create({
    title: 'Title 3',
    body: 'Body 3',
    approved: false
})
.then(article => {
    console.log(article)
})
.catch(err => {
    console.log(err)
})