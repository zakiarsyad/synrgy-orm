const { Article } = require('./models')

Article.update({
    approved: false
}, {
    where: {
        id: 1
    }
})
.then(article => {
    console.log(article)
})
.catch(err => {
    console.log(err)
})